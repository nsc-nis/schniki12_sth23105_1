function data()
return {
	info = {
		minorVersion = 1,
		severityAdd = "NONE",
		severityRemove = "WARNING",
		name = _("et23105_mod_name"),
		description = _("et23105_mod_desc"),
		tags = {"europe", "austria", "multiple unit"},
		authors = {
			{
				name = "Schniki12",
				role = "CREATOR",
			},
		},
	},
}
end
