function data()
return {
    de = {
        ["et23105_mod_name"] = "StH ET 23.105 - 106",
        ["et23105_mod_desc"] = "Die Triebwagen 23.105 und 23.106 der Traunseebahn",
        ["et23105_altlack_name"] = "StH ET 23.105 Altlack",
        ["et23105_altlack_desc"] = "ET 23.105 im alten rot-beigen Lack\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\n1978 kaufte Stern&Hafferl von der Trogenerbahn (Schweiz) drei 1954 gebaute Triebwagen. Der Umbau war ziemlich aufwendig (Änderung der Türanordnung, Anheben des Wagenbodens im Mittelteil, ...), sodass der erste Triebwagen erst am 3.September 1982 als ET 23.105 in Betrieb gehen konnte. Der zweite Triebwagen erhielt neue Stirnfenster und verbesserte Sitze und ging am 25.Juli 1984 in Betrieb. Diese beiden Triebwagen befanden sich noch bis 2016 im Planverkehr. Der dritte Triebwagen wurde nicht mehr umgebaut, er diente als Ersatzteilspender und wurde 1988 verschrottet.",
        ["et23105_neulack_name"] = "StH ET 23.105 Neulack",
        ["et23105_neulack_desc"] = "ET 23.105 in der neuen Stern&Hafferl-Lackierung\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\n1978 kaufte Stern&Hafferl von der Trogenerbahn (Schweiz) drei 1954 gebaute Triebwagen. Der Umbau war ziemlich aufwendig (Änderung der Türanordnung, Anheben des Wagenbodens im Mittelteil, ...), sodass der erste Triebwagen erst am 3.September 1982 als ET 23.105 in Betrieb gehen konnte. Der zweite Triebwagen erhielt neue Stirnfenster und verbesserte Sitze und ging am 25.Juli 1984 in Betrieb. Diese beiden Triebwagen befanden sich noch bis 2016 im Planverkehr. Der dritte Triebwagen wurde nicht mehr umgebaut, er diente als Ersatzteilspender und wurde 1988 verschrottet.",
        ["et23105_neuesLogo_name"] = "StH ET 23.105 neues Logo",
        ["et23105_neuesLogo_desc"] = "ET 23.105 mit dem neuen Stern&Hafferl-Schriftzug als Logo\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\n1978 kaufte Stern&Hafferl von der Trogenerbahn (Schweiz) drei 1954 gebaute Triebwagen. Der Umbau war ziemlich aufwendig (Änderung der Türanordnung, Anheben des Wagenbodens im Mittelteil, ...), sodass der erste Triebwagen erst am 3.September 1982 als ET 23.105 in Betrieb gehen konnte. Der zweite Triebwagen erhielt neue Stirnfenster und verbesserte Sitze und ging am 25.Juli 1984 in Betrieb. Diese beiden Triebwagen befanden sich noch bis 2016 im Planverkehr. Der dritte Triebwagen wurde nicht mehr umgebaut, er diente als Ersatzteilspender und wurde 1988 verschrottet.",
        ["et23106_altlack_name"] = "StH ET 23.106 Altlack",
        ["et23106_altlack_desc"] = "ET 23.106 im alten rot-beigen Lack. Unterschiede zum ET 23.105: neuere Sitze, geänderte Sitze im Mittelteil, geänderte Front mit neueren Lichtern. Auch die Türen waren beim alten Lack anders Lackiert\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\n1978 kaufte Stern&Hafferl von der Trogenerbahn (Schweiz) drei 1954 gebaute Triebwagen. Der Umbau war ziemlich aufwendig (Änderung der Türanordnung, Anheben des Wagenbodens im Mittelteil, ...), sodass der erste Triebwagen erst am 3.September 1982 als ET 23.105 in Betrieb gehen konnte. Der zweite Triebwagen erhielt neue Stirnfenster und verbesserte Sitze und ging am 25.Juli 1984 in Betrieb. Diese beiden Triebwagen befanden sich noch bis 2016 im Planverkehr. Der dritte Triebwagen wurde nicht mehr umgebaut, er diente als Ersatzteilspender und wurde 1988 verschrottet.",
        ["et23106_neulack_name"] = "StH ET 23.106 Neulack",
        ["et23106_neulack_desc"] = "ET 23.106 in der neuen Stern&Hafferl-Lackierung. Unterschiede zum ET 23.105: neuere Sitze, geänderte Sitze im Mittelteil, geänderte Front mit neueren Lichtern.\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\n1978 kaufte Stern&Hafferl von der Trogenerbahn (Schweiz) drei 1954 gebaute Triebwagen. Der Umbau war ziemlich aufwendig (Änderung der Türanordnung, Anheben des Wagenbodens im Mittelteil, ...), sodass der erste Triebwagen erst am 3.September 1982 als ET 23.105 in Betrieb gehen konnte. Der zweite Triebwagen erhielt neue Stirnfenster und verbesserte Sitze und ging am 25.Juli 1984 in Betrieb. Diese beiden Triebwagen befanden sich noch bis 2016 im Planverkehr. Der dritte Triebwagen wurde nicht mehr umgebaut, er diente als Ersatzteilspender und wurde 1988 verschrottet.",
        ["et23106_neuesLogo_name"] = "StH ET 23.106 neues Logo",
        ["et23106_neuesLogo_desc"] = "ET 23.106 mit dem neuen Stern&Hafferl-Schriftzug als Logo. Unterschiede zum ET 23.105: neuere Sitze, geänderte Sitze im Mittelteil, geänderte Front mit neueren Lichtern.\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\n1978 kaufte Stern&Hafferl von der Trogenerbahn (Schweiz) drei 1954 gebaute Triebwagen. Der Umbau war ziemlich aufwendig (Änderung der Türanordnung, Anheben des Wagenbodens im Mittelteil, ...), sodass der erste Triebwagen erst am 3.September 1982 als ET 23.105 in Betrieb gehen konnte. Der zweite Triebwagen erhielt neue Stirnfenster und verbesserte Sitze und ging am 25.Juli 1984 in Betrieb. Diese beiden Triebwagen befanden sich noch bis 2016 im Planverkehr. Der dritte Triebwagen wurde nicht mehr umgebaut, er diente als Ersatzteilspender und wurde 1988 verschrottet.",
        ["et23105_menu_name"] = "Stern&Hafferl ET 23.105 - 106 (Traunseebahn)",
    },
    en = {
        ["et23105_mod_name"] = "StH ET 23.105 - 106",
        ["et23105_mod_desc"] = "The electric railcars 23.105 and 23.106 of the Austrian railway 'Traunseebahn'",
        ["et23105_altlack_name"] = "StH ET 23.105 old painting",
        ["et23105_altlack_desc"] = "ET 23.105 in the old red-beige painting\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\nIn 1978, the company Stern&Hafferl bought three electric railcars, which were built in 1954, from the 'Trogenerbahn' railway in Switzerland. The rebuil was quite complex and involved modification of the door arrangement and raising of the car floor in the center sektion. Because of that, the first railcar, which was given the number ET23.105, couldn't be used until September 3, 1982.The second railcar got new windows on the front and improved seats. It entered service on July 25, 1984. These two railcars remaind in service until 2016. The third railcar was never rebuilt, it was beeing used as a spare parts donor and was scrapped in 1988.",
        ["et23105_neulack_name"] = "StH ET 23.105 new painting",
        ["et23105_neulack_desc"] = "ET 23.105 in the new Stern&Hafferl painting\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\nIn 1978, the company Stern&Hafferl bought three electric railcars, which were built in 1954, from the 'Trogenerbahn' railway in Switzerland. The rebuil was quite complex and involved modification of the door arrangement and raising of the car floor in the center sektion. Because of that, the first railcar, which was given the number ET23.105, couldn't be used until September 3, 1982.The second railcar got new windows on the front and improved seats. It entered service on July 25, 1984. These two railcars remaind in service until 2016. The third railcar was never rebuilt, it was beeing used as a spare parts donor and was scrapped in 1988.",
        ["et23105_neuesLogo_name"] = "StH ET 23.105 new Logo",
        ["et23105_neuesLogo_desc"] = "ET 23.105 with the new Stern&Hafferl logo\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\nIn 1978, the company Stern&Hafferl bought three electric railcars, which were built in 1954, from the 'Trogenerbahn' railway in Switzerland. The rebuil was quite complex and involved modification of the door arrangement and raising of the car floor in the center sektion. Because of that, the first railcar, which was given the number ET23.105, couldn't be used until September 3, 1982.The second railcar got new windows on the front and improved seats. It entered service on July 25, 1984. These two railcars remaind in service until 2016. The third railcar was never rebuilt, it was beeing used as a spare parts donor and was scrapped in 1988.",
        ["et23106_altlack_name"] = "StH ET 23.106 old painting",
        ["et23106_altlack_desc"] = "ET 23.106 in the old red-beige painting. Differences to ET23.105: newer Seats, modified seats in the middle sektion, modified front with newer lights.\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\nIn 1978, the company Stern&Hafferl bought three electric railcars, which were built in 1954, from the 'Trogenerbahn' railway in Switzerland. The rebuil was quite complex and involved modification of the door arrangement and raising of the car floor in the center sektion. Because of that, the first railcar, which was given the number ET23.105, couldn't be used until September 3, 1982.The second railcar got new windows on the front and improved seats. It entered service on July 25, 1984. These two railcars remaind in service until 2016. The third railcar was never rebuilt, it was beeing used as a spare parts donor and was scrapped in 1988.",
        ["et23106_neulack_name"] = "StH ET 23.106 new painting",
        ["et23106_neulack_desc"] = "ET 23.106 in the new Stern&Hafferl painting. Differences to ET23.105: newer Seats, modified seats in the middle sektion, modified front with newer lights.\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\nIn 1978, the company Stern&Hafferl bought three electric railcars, which were built in 1954, from the 'Trogenerbahn' railway in Switzerland. The rebuil was quite complex and involved modification of the door arrangement and raising of the car floor in the center sektion. Because of that, the first railcar, which was given the number ET23.105, couldn't be used until September 3, 1982.The second railcar got new windows on the front and improved seats. It entered service on July 25, 1984. These two railcars remaind in service until 2016. The third railcar was never rebuilt, it was beeing used as a spare parts donor and was scrapped in 1988.",
        ["et23106_neuesLogo_name"] = "StH ET 23.106 new logo",
        ["et23106_neuesLogo_desc"] = "ET 23.106 with the new Stern&Hafferl logo. Differences to ET23.105: newer Seats, modified seats in the middle sektion, modified front with newer lights.\n" ..
                                   "-------------------------------------------------------------\n" ..
                                   "\nIn 1978, the company Stern&Hafferl bought three electric railcars, which were built in 1954, from the 'Trogenerbahn' railway in Switzerland. The rebuil was quite complex and involved modification of the door arrangement and raising of the car floor in the center sektion. Because of that, the first railcar, which was given the number ET23.105, couldn't be used until September 3, 1982.The second railcar got new windows on the front and improved seats. It entered service on July 25, 1984. These two railcars remaind in service until 2016. The third railcar was never rebuilt, it was beeing used as a spare parts donor and was scrapped in 1988.",
        ["et23105_menu_name"] = "Stern&Hafferl ET 23.105 - 106 (Traunseebahn)",
    },
}
end
