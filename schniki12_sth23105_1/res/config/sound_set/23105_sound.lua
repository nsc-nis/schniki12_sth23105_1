local audioutil = require "audioutil"
local soundsetutil = require "soundsetutil"

local clackNames = {
	"vehicle/clack/old/part_1.wav",
	"vehicle/clack/old/part_2.wav",
	"vehicle/clack/old/part_3.wav",
	"vehicle/clack/old/part_4.wav",
	"vehicle/clack/old/part_5.wav",
	"vehicle/clack/old/part_6.wav",
	"vehicle/clack/old/part_7.wav",
	"vehicle/clack/old/part_8.wav",
	"vehicle/clack/old/part_9.wav",
	"vehicle/clack/old/part_10.wav"
}

function data()

local data = soundsetutil.makeSoundSet()

soundsetutil.addTrackParam01(data, "vehicle/train/23105_umformer.wav", 10.0,
		{ { 0.0, 4.0 }, { 0.3, 4.0 }, { 0.6, 0.0 } },
		{ { 0.0, 1.0 }, { 0.02, 0.88 }, { 1.0, 0.88 } }, "speed01")

soundsetutil.addTrackParam01(data, "vehicle/train/23105_tief.wav", 20.0,
		{ { 0.0, 0.0 }, { 0.2, 0.6 }, { 0.6, 0.8 }, { 0.8, 0.0 } },
		{ { 0.0, 0.07 }, { 0.9, 1.7 } }, "speed01")
		
soundsetutil.addTrackParam01(data, "vehicle/train/23105_hoch.wav", 20.0,
		{ { 0.55, 0.0 }, { 0.75, 1.8 }, { 1.0, 0.8 } },
		{ { 0.55, 0.7 }, { 1.0, 1.25 } }, "speed01")
		
soundsetutil.addTrackParam01(data, "vehicle/train/23105_leer.wav", 20.0,
		{ { 0.8, 0.0 }, { 1.0, 0.7 } },
		{ { 0.8, 1.0 }, { 1.0, 1.25 } }, "speed01")
		
soundsetutil.addTrackSqueal(data, "vehicle/train/23105_kurve.wav", 20.0)
soundsetutil.addTrackBrake(data, "vehicle/train_electric_old/_brakes.wav", 20.0, .5)

soundsetutil.addEventClacks(data, clackNames, 15.0, 10.0)
soundsetutil.addEvent(data, "horn", { "vehicle/train/btbsetb_horn.wav" }, 50.0)

return data

end
