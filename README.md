# StH ET23.105 & ET23.106
## About
This mod adds the trains ET 23.105 and ET 23.106 from the "Traunseebahn" in Austria to the game [TransportFever2](https://www.transportfever2.com/).  
## Repository
This repository contains all the files needed to create this mod. (3D-model in Blender, textures, etc.) 
## Download
You can download this mod on the [TransportFever-Forum](https://www.transportfever.net/filebase/entry/6635-sth-et-23-105-106/), the [Steam-Workshop](https://steamcommunity.com/workshop/filedetails/?id=2747849202) and on the [releases-page](https://gitlab.com/dev-nis/schniki12_sth23105_1/-/releases) of this repository